from __future__ import print_function
# The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from builtins import str
from builtins import input
from invoke import task
from invoke.exceptions import UnexpectedExit
import os
import sys
import hgapi
from git import Repo as GitRepo
from hgapi import HgException
from blessings import Terminal
import platform
from os import chdir
try:
    from configparser import ConfigParser
except ImportError:
    from ConfigParser import ConfigParser
from packaging import version as package_version
from multiprocessing import Process, Queue, cpu_count, queues
import io
from contextlib import redirect_stdout, redirect_stderr


DEFAULT_BRANCH = {
    'git': 'master',
    'hg': 'default'
}

MODULES_PATH = '%s/trytond/modules'

MONO_REPOS = ['CORE', 'CORE_MODULES']
IGNORE_REPOS = [
    'trytond', 'tryton', 'sao', 'proteus', 'dvconfig', 'config'] + MONO_REPOS

TYPE_REPOS = {
    'gitlab.': 'git',
    'github.com': 'git',
    'hg.tryton.org': 'hg'
}

SHOW_CURRENT = None

CPU_COUNT = cpu_count()


def read_cp_list(data):
    return list(filter(None, [x.strip() for x in data.splitlines()]))


@task(help={'config': "Config file",
            'repos': "Repository names separated by commas",
            'path': "Filters repositories with given path",
            'branch': "Branch name"},
      aliases=['update'])
def checkout(ctx, config="config/own.cfg", repos=None, path=None,
             branch=None, clean=False):
    """Make a checkout from one or all repositories specified in
    a config file"""
    cp = ConfigParser()
    cp.read(config)
    if repos:
        _repos = str(repos).split(',')
    else:
        _repos = cp.sections()
    for section in _repos:
        if path and cp.get(section, 'path') != path:
            continue
        _checkout_repo(ctx, section, cp, branch, clean)


def _checkout_repo(ctx, section, cp, branch=None, clean=False):
    """Checkout a git or hg repository"""
    repository = _get_repo(section, cp, 'checkout')
    t = Terminal()
    msg = []
    msg.insert(0, '\n%sCheckout repository "%s" to "%s"' % (
        t.green, section, branch or repository['branch']))
    msg.append(t.normal + '')
    print(' '.join(msg))
    repository['function'](ctx,
        repository, branch or repository['branch'], clean)


def _hg_checkout(ctx, repository, branch, clean):
    repo = hgapi.Repo(repository['repo_path'])
    revision = branch
    if branch not in repo.get_branch_names():
        revision = repository['main_branch']
        print('Branch "%s" does not exists! Checking out "%s" ' % (
            branch, revision))
    if repo.get_branch_names():
        repo.hg_update(revision, clean=clean)


def _git_checkout(ctx, repository, branch, clean):
    cwd = os.getcwd()
    os.chdir(repository['repo_path'])
    command = (
        '(git show-ref --quiet ' + branch + ' && git checkout ' + branch + ') '
        + '|| (git cat-file -e ' + branch + ' && git checkout ' + branch + ') '
        + '|| (git checkout ' + repository['main_branch'] + '; '
        + 'echo "Branch %s does not exists! Checking out %s")' % (branch,
            repository['main_branch']))
    output = ctx.run(command, hide='both')
    if output.stderr:
        print(output.stderr.split('\n')[0])
    print(output.stdout, end='')
    os.chdir(cwd)


@task(help={'config': "Config file",
            'repos': "Repository names separated by commas",
            'path': "Filters repositories with given path"})
def clone(ctx, config="config/own.cfg", repos=None, path=None, verbose=False):
    """Make a clone from one or all repositories specified in a config file"""
    cp = ConfigParser()
    cp.read(config)
    if repos:
        _repos = str(repos).split(',')
    else:
        _repos = cp.sections()
    for section in _repos:
        if path and cp.get(section, 'path') != path:
            continue
        if _clone_repo(ctx, section, cp, verbose):
            _checkout_repo(ctx, section, cp)


def _clone_repo(ctx, repo, cp, verbose):
    """Cloning a git or hg repository"""
    t = Terminal()
    msg = []
    msg.insert(0, '\n%sClone repository "%s"' % (t.green, repo))
    msg.append(t.normal + '')
    print(' '.join(msg))

    repository = _get_repo(repo, cp, 'clone')
    if os.path.exists(repository['repo_path']) and os.listdir(
            repository['repo_path']):
        print('Repository "%s" already exists!' % repository['repo_path'])
        return False
    repository['function'](ctx, repository)
    if verbose:
        _show_changes_repo(repo, repository)
    return True


def _hg_clone(ctx, repository):
    extended_args = ['--pull']
    hgapi.hg_clone(repository['url'], repository['repo_path'], *extended_args)


def _git_add_sparse_checkout_paths(ctx, repository):
    # Add if not exist in sparse-checkout config.
    add_sparse_cmd = (
        "grep -qxF '%s' .git/info/sparse-checkout "
        "|| echo '%s' >> .git/info/sparse-checkout")
    for sub_path in repository.get('modules', []):
        sub_path = 'modules/%s' % sub_path
        ctx.run(add_sparse_cmd % (sub_path, sub_path))
    for sub_path in repository.get('packages', []):
        ctx.run(add_sparse_cmd % (sub_path, sub_path))


def _git_clone(ctx, repository):
    if 'modules' in repository or 'packages' in repository:
        cwd = os.getcwd()
        ctx.run('mkdir %s' % repository['repo_path'])
        os.chdir(repository['repo_path'])
        init_branch = (repository['depth'] and repository['branch']
            or repository['main_branch'])
        ctx.run('git init -b %s' % init_branch)
        ctx.run('git remote add origin %s' % repository['url'])
        ctx.run('git config core.sparsecheckout true')
        ctx.run('touch .git/info/sparse-checkout')
        _git_add_sparse_checkout_paths(ctx, repository)
        depth = repository['depth']
        if depth:
            ctx.run('git pull --depth=%s origin %s' % (depth, init_branch))
        else:
            ctx.run('git pull origin %s' % init_branch)
            ctx.run('git fetch origin %s' % repository['branch'])
        os.chdir(cwd)

    else:
        GitRepo.clone_from(repository['url'], repository['repo_path'])


def _clear_queue(queue):
    try:
        while True:
            queue.get_nowait()
    except queues.Empty:
        pass


@task(help={'config': "Config file",
            'repos': "Repository names separated by commas",
            'path': "Filters repositories with given path",
            'verbose': "Activates verbose mode",
            'last-repo': "Starts pull batch process from the given repo"})
def pull(ctx, config="config/own.cfg", repos=None, path=None, verbose=False,
        last_repo=None):
    """Make a pull from one or all repositories specified in a config file"""
    cp = ConfigParser()
    cp.read(config)
    if repos:
        _repos = str(repos).split(',')
    else:
        _repos = cp.sections()
    queue = Queue()
    found = not bool(last_repo)
    for section in _repos:
        repo_path = cp.get(section, 'path')
        if not found:
            found = (section == last_repo)
        if path and repo_path != path:
            continue
        if found:
            queue.put(section)

    procs = []
    for x in range(CPU_COUNT):
        proc = Process(target=_thread_pull, args=[ctx, queue, cp])
        proc.start()
        procs.append(proc)

    for proc in procs:
        proc.join()


def _thread_pull(ctx, queue, cp):
    while not queue.empty():
        repo = queue.get()
        sf = io.StringIO()
        with redirect_stdout(sf), redirect_stderr(sf):
            try:
                _pull_repo(ctx, repo, cp)
            except Exception as e:
                _clear_queue(queue)
                print(e)
        out_ = sf.getvalue()
        print(out_)


def _pull_repo(ctx, repo, cp):
    """Pulling a git or hg repository"""
    t = Terminal()
    msg = []
    msg.insert(0, '\n%sPulling repository "%s"' % (t.green, repo))
    msg.append(t.normal + '')
    print(' '.join(msg))
    repository = _get_repo(repo, cp, 'pull')
    repository['function'](ctx, repository)


def _hg_pull(ctx, repository, version=None):
    repo = hgapi.Repo(repository['repo_path'])
    cwd = os.getcwd()
    os.chdir(repository['repo_path'])
    try:
        if repository['only_branch']:
            ctx.run("hg pull {} -b '{}'".format(
                repository['url'], repository['branch']))
        else:
            ctx.run('hg pull ' + repository['url'])
    except Exception as ex:
        os.chdir(cwd)
        raise ex
    os.chdir(cwd)
    revision = version or repository['branch']
    if revision not in repo.get_branch_names():
        revision = repository['main_branch']
    print("Update to branch '%s'" % revision)

    os.chdir(repository['repo_path'])
    ctx.run("hg update '%s'" % revision)
    os.chdir(cwd)


def _git_pull(ctx, repository, version=None):
    revision = version or repository['branch']
    cwd = os.getcwd()
    os.chdir(repository['repo_path'])

    global SHOW_CURRENT
    if not SHOW_CURRENT:
        git_version_execute = str(ctx.run(
            "git --version | tr -s ' ' '\n' | tail -n 1", hide=True))
        git_version = git_version_execute.replace('\n', ',').split(',')[-3]
        # --show-current only works with git >= 2.22.0
        SHOW_CURRENT = (package_version.parse(git_version)
            >= package_version.parse('2.22.0'))

    command = 'git pull'
    if repository['only_branch']:
        command += ' %s %s' % (repository['url'], revision)
    if 'modules' in repository or 'packages' in repository:
        _git_add_sparse_checkout_paths(ctx, repository)
        if repository['depth']:
            command += ' --depth=%s' % repository['depth']
        command += ' origin %s; git read-tree -mu HEAD' % revision
    if SHOW_CURRENT:
        output = ctx.run('git branch --show-current', hide='out')
    else:
        print('Option --show-current not available. Using rev-parse ...')
        output = ctx.run('git rev-parse --abbrev-ref HEAD', hide='out')
    current_branch = output.stdout.replace('\n', '')
    if current_branch:
        try:
            ctx.run('git fetch origin %s' % current_branch, hide='both')
        except UnexpectedExit:
            if current_branch == 'master':
                command = 'git checkout main; %s' % command
        ctx.run(command)
    else:
        print("Detached on '%s'" % repository['branch'])
        print('Nothing to pull.')
    os.chdir(cwd)
    if output.stdout:
        _git_checkout(ctx, repository, revision, False)


@task(help={'config': "Config file",
            'gitOnly': "List only git repositories",
            'unstable': "Ignore unestable config files",
            'verbose': "Verbose"})
def repo_list(ctx, config=None, gitOnly=False, unstable=True, verbose=False):
    """List repositories"""
    # t = Terminal()
    cf = _read_config_file(config, unstable=unstable)
    repos = {
        'git': [],
        'hg': []
    }
    for section in cf.sections():
        repo = cf.get(section, 'repo')
        url = cf.get(section, 'url')
        repo_path = cf.get(section, 'path')
        repos[repo] += [(section, url, repo_path)]

    if gitOnly:
        del repos['hg']

    for key, values in repos.items():
        print("Repositories in  " + key, file=sys.stderr)
        for val in values:
            name, url, repo_path = val
            if not verbose:
                print(name, file=sys.stderr)
            else:
                print(name, repo_path, url, file=sys.stderr)


def _print_revisions(message, module, revisions):
    if revisions:
        t = Terminal()
        msg = []
        msg.insert(0, '\n%s %s' % (t.yellow, message))
        msg.insert(1, '\n%s [%s]' % (t.yellow, module))
        msg.insert(2, '=' * (len(module) + 2))
        msg.append(t.normal + '')
        print('\n '.join(msg))
        for revision in revisions:
            print('  changeset:  %s:%s' % (
                revision.rev, revision.node))
            print('  branch:  %s' % revision.branch)
            if revision.tags:
                print('  tag:  %s' % revision.tags)
            print('  user:  %s' % revision.author)
            print('  date:  %s' % revision.date)
            print('  summary:  %s\n\n' % revision.desc)


def _hg_get_my_branch(repository, version=None):
    repo = hgapi.Repo(repository['repo_path'])
    return repo.hg_branch(version)


def _show_changes_repo(module, repo):
    push_message = ("----Changes not found in the destination repository"
        " (push is needed)----")
    pull_message = ("----Changes not found in the local repository"
                    " (pull is needed)----")
    repo_api = hgapi.Repo(repo['repo_path'])
    my_branch = _hg_get_my_branch(repo)
    try:
        revisions = repo_api.hg_outgoing()
        revisions_branch = [r for r in revisions if r.branch == my_branch]
        _print_revisions(push_message, module, revisions_branch)
        revisions = repo_api.hg_incoming()
        revisions_branch = [r for r in revisions if r.branch == my_branch]
        _print_revisions(pull_message, module, revisions_branch)
    except HgException:
        pass


@task(help={
    'config': "Config file",
    'path': 'Path to repositories',
    'repos': "Repository names separated by commas",
    'unstable': "Ignore unestable config files",
    'verbose': "Verbose"},
    aliases=['st'])
def status(ctx, config="config/own.cfg", path='modules', repos=None,
        unstable=True, verbose=False):
    """Status from one or all repositories specified in a config file"""
    cp = _read_config_file(config, unstable=unstable)
    if repos:
        _repos = str(repos).split(',')
    else:
        _repos = cp.sections()

    for section in _repos:
        repo_path = cp.get(section, 'path')
        if repo_path == path:
            repo = _get_repo(section, cp, 'status')
            if not os.path.exists(repo['repo_path']):
                print(('Missing repository: "%s"' %
                    repo['repo_path']), file=sys.stderr)
                return
            repo['function'](section, repo['repo_path'], verbose, repo['url'])
            if verbose:
                _show_changes_repo(section, repo)


@task(help={
    'config': "Config file",
    'database': "Database",
    'backup': 'Backup to restore',
    'update': 'Update database'
    })
def create_db(ctx, database, config="etc/trytond.conf", backup=None,
        update=True):
    """Create a new Database or restore from a backup"""
    result = ctx.run("psql -lqt | cut -d \| -f 1 | grep -qw {}".format(
        database))
    if result.stdout == database:
        print("Error database '{}' already exists!".format(database))
        exit(1)
    ctx.run("createdb {} -U postgres -O trytond".format(database))
    print("Database created!")

    if backup:
        print("Restoring database...(this can take a while)")
        ctx.run("pg_restore --username trytond --format custom -n public "
            "-O -d {} {}".format(database, backup))

    if not backup or update:
        print("Creating Tryton tables...(this can take a while)")
        ctx.run("trytond-admin -c {} -d {} --all".format(config, database))

    if backup:
        print("Changing admin password...")
        setup_db(ctx, config, database, warning_db_name=False,
            rename_companies=False)

    print("Database '{}' was created successfully!".format(database))


def _print_status(module, files, extra=None):
    t = Terminal()
    status_key_map = {
        'A': 'Added',
        'M': 'Modified',
        'R': 'Removed',
        '!': 'Deleted',
        '?': 'Untracked',
        'D': 'Deleted',
    }

    status_key_color = {
        'A': t.green,
        'M': t.yellow,
        'R': t.red,
        '!': t.red,
        '=': t.blue,
        'D': t.red,
        '?': t.red,
    }

    msg = []
    for key, value in files.items():
        tf = status_key_map.get(key)
        col = status_key_color.get(key, t.normal)
        for f in value:
            msg.append(col + " %s (%s):%s " % (tf, key, f))
    if extra:
        msg.insert(0, '%s %s\n' % (t.yellow, extra))
    if msg:
        msg.insert(0, "[%s]" % module)
        msg.insert(1, '=' * (len(module) + 2))
        msg.append(t.normal + '')
        print('\n'.join(msg))


def _hg_status(module, path, verbose, url):
    """Hg status"""

    repo = hgapi.Repo(path)
    actual_url = str(repo.config('paths', 'default')).rstrip('/')
    url = str(url).rstrip('/')
    extra = None
    if actual_url != url:
        extra = 'Repo URL differs:\n\t%s\n\t%s' % (actual_url, url)

    st = repo.hg_status(empty=True)
    _print_status(module, st, extra)
    return st


def _git_status(module, path, verbose, url):
    """Git status"""

    repo = GitRepo(path)
    config = repo.config_reader()
    config.read()
    actual_url = config.get_value('remote "origin"', 'url')
    extra = None
    if actual_url != url:
        extra = 'Repo URL differs:\n\t%s\n\t%s' % (actual_url, url)
    diff = repo.index.diff(None)
    files = {}
    for change in diff.change_type:
        files[change] = []

    if diff:
        for change in diff.change_type:
            for d in diff.iter_change_type(change):
                files[change].append(d.a_blob.path)
    _print_status(module, files, extra)
    return files


def _get_repo(section, config, function=None):
    section_path = config.get(section, 'path')
    repository = {
        'type': config.get(section, 'repo'),
        'url': config.get(section, 'url'),
        'path': os.path.join(section_path, section),
        'repo_path': os.path.join(section_path, section)}
    for k, v in TYPE_REPOS.items():
        if k in repository['url']:
            repository['type'] = v
            break
    if config.has_option(section, 'repo_path'):
        repository['repo_path'] = config.get(section, 'repo_path')
    repository['branch'] = DEFAULT_BRANCH[repository['type']]
    if config.has_option(section, 'branch'):
        repository['branch'] = config.get(section, 'branch')
    repository['main_branch'] = DEFAULT_BRANCH[repository['type']]
    if config.has_option(section, 'main_branch'):
        repository['main_branch'] = config.get(section, 'main_branch')
    repository['function'] = None
    if function:
        try:
            repository['function'] = eval('_%s_%s' % (
                repository['type'], function))
        except Exception:
            pass
    repository['only_branch'] = False

    if section in MONO_REPOS:
        repository['main_branch'] = 'main'
        repository['path'] = config.get(section, 'path', fallback='core')
        repository['repo_path'] = repository['path']
        for sec in ['modules', 'packages']:
            if config.has_option(section, sec):
                repository[sec] = read_cp_list(config.get(section, sec))
        repository['depth'] = config.get(section, 'depth', fallback=1)
    return repository


def _read_config_file(config_file=None, type='repos', unstable=True):
    assert type in ('repos', 'patches', 'all'), "Invalid 'type' param"

    cp = ConfigParser()
    if config_file is not None:
        cp.read_file(open(config_file))
    else:
        for r, d, f in os.walk("./config"):
            for files in f:
                if not files.endswith(".cfg"):
                    continue
                if not unstable and files.endswith("-unstable.cfg"):
                    continue
                cp.read_file(open(os.path.join(r, files)))

    if type == 'all':
        return cp
    for section in cp.sections():
        is_patch = (cp.has_option(section, 'patch')
                and cp.getboolean(section, 'patch'))
        if type == 'repos' and is_patch:
            cp.remove_section(section)
        elif type == 'patches' and not is_patch:
            cp.remove_section(section)
    return cp


def _get_modules_data(cp):
    path, values = None, []
    if cp.has_section('CORE_MODULES'):
        path = cp.get('CORE_MODULES', 'path', fallback='core_modules')
        values = read_cp_list(cp.get('CORE_MODULES', 'modules', fallback=''))
    elif cp.has_section('CORE'):
        path = cp.get('CORE', 'path', fallback='core')
        values = read_cp_list(cp.get('CORE', 'modules', fallback=''))
    return path, values


@task(help={'config': "Config file",
            'repos': "Repository names separated by commas",
            'path': "Filters repositories with given path",
            'force': "Forces task to create link again",
            'server': "Trytond server"})
def create_links(ctx, config='config/own.cfg', repos=None, path=None,
        force=False, server='trytond'):
    """
    Create symbolic links in trytond server modules
    folder from an specific repository folder
    """
    cp = ConfigParser()
    cp.read(config)
    if cp.has_section('CORE'):
        core_path = cp.get('CORE', 'path', fallback='core')
        if server == 'trytond':
            server = os.path.join(core_path, server)
    else:
        core_path = None

    core_modules_path, core_modules = _get_modules_data(cp)

    current_path = os.getcwd()
    destination = '{0}/{server_path}/{2}'
    text = 'ln -s -f {0}/{1}/{2} ' + destination
    if platform.system().upper() == 'WINDOWS':
        text = 'mklink /d "' + destination + '" "{0}/{1}/{2}"'
    destination_path = MODULES_PATH % server
    if repos:
        _repos = str(repos).split(',')
    else:
        _repos = cp.sections() + core_modules
    for section in _repos:
        if section in IGNORE_REPOS:
            continue
        if section in core_modules:
            path_ = os.path.join(core_modules_path, 'modules')
        else:
            path_ = cp.get(section, 'path')
        if path and path_ != path:
            continue
        print('Creating symbolic link of "%s"' % section)
        if _check_destination(ctx, destination.format(
            current_path, path_, section,
                server_path=destination_path), force):
            ctx.run(text.format(current_path, path_, section,
                server_path=destination_path))


@task(help={'config': "Config file",
            'repos': "Repository names separated by commas",
            'path': "Filters repositories with given path",
            'all': "Remove all module links",
            'server': "Trytond server"})
def delete_links(ctx, config='config/own.cfg', repos=None, path=None,
        all=False, server='trytond'):
    """
    Delete symbolic links of trytond server modules
    folder from an specific repository folder
    """
    cp = ConfigParser()
    cp.read(config)
    if cp.has_section('CORE'):
        core_path = cp.get('CORE', 'path', fallback='core')
        if server == 'trytond':
            server = os.path.join(core_path, server)
    else:
        core_path = None

    _, core_modules = _get_modules_data(cp)

    current_path = os.getcwd()
    text = 'rm {0}/{server_path}/{1}'
    if platform.system().upper() == 'WINDOWS':
        text = 'rmdir "{0}/{server_path}/{1}"'
    destination_path = MODULES_PATH % server
    if repos:
        _repos = str(repos).split(',')
    elif all:
        _repos = [item for item in os.listdir(destination_path)
                  if os.path.islink('%s/%s' % (destination_path, item))]
    else:
        _repos = cp.sections()
        _repos += core_modules
    for section in _repos:
        if section in IGNORE_REPOS:
            continue
        if not all:
            if section in core_modules:
                path_ = os.path.join(core_path, 'modules')
            else:
                path_ = cp.get(section, 'path')
            if path and path_ != path:
                continue
        print('Deleting symbolic link of "%s"' % section)
        ctx.run(
            text.format(current_path, section, server_path=destination_path))


def _check_destination(ctx, destination, force):
    msg = ''
    if not os.path.exists(destination):
        return 'Destination file does not exists.'
    if os.path.islink(destination):
        if force:
            ctx.run('rm %s' % destination)
        else:
            msg = 'Destination path already exists as symbolic link'
    elif os.path.isdir(destination):
        msg = 'Destination path already exists as directory'
    elif os.path.isfile(destination):
        msg = 'Destination path already exists as file'

    if msg != '':
        print(msg.format(destination))
    return msg == ''


@task(help={'config': "Config file",
            'remote': "Forces pull from remote repos"})
def deploy(ctx, config='config/own.cfg', remote=False):
    """
    Activates a deployment cfg file
    """
    delete_links(ctx, config=config, all=True)
    if remote:
        pull(ctx, config=config)
    checkout(ctx, config=config)
    ctx.run('find ./trytond -name "*.pyc" -exec rm -rf {} \;')
    create_links(ctx, config=config)


@task(help={'config': "Config file",
            'path': "Filters repositories with given path"
            })
def install_modules(ctx, config='config/own.cfg', path=None):
    """
    Install modules specified on a deployment cfg file using
    python setup.py install
    """
    t = Terminal()
    cp = ConfigParser()
    cp.read(config)
    _repos = cp.sections()
    len_repos = len(_repos)
    i = 1
    for section in _repos:
        if path and cp.get(section, 'path') != path:
            continue
        cwd = os.getcwd()
        chdir('{}/{}'.format(cp.get(section, 'path'), section))
        msg = []
        message = '#' * 10 + ' installing "{}" ({}/{})'.format(
            section, i, len_repos) + '#' * 10
        msg.insert(0, '\n {} {}'.format(t.green, message))
        msg.append(t.normal + '')
        print(''.join(msg))
        ctx.run('python setup.py install')
        i += 1
        os.chdir(cwd)


def setup_db(ctx, configfile, database_name, warning_db_name=True,
        rename_companies=True):

    def warning_dabase_name(database_name):
        if database_name == 'tryton':
            print('Can not setup "tryton" as test database')
            exit(0)
        test_denoms = ['prueba', 'test', 'aceptac']
        warning = True
        for td in test_denoms:
            if td in database_name:
                warning = False

        if warning:
            return input('%s does not appear to be a test database.'
                ' Cancel (y/N)? ' % database_name) != 'N'
        return False

    if warning_db_name and warning_dabase_name(database_name):
        exit(0)

    # Change password to  admin
    ctx.run("trytond-admin -c {} -d {} -p".format(config, database))

    # Rename companies
    if rename_companies:
        raise NotImplementedError()
