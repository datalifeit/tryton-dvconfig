#!/usr/bin/env python
# The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
import os
from setuptools import setup, find_packages


def read(fname):
    return open(os.path.join(os.path.dirname(__file__), fname)).read()


setup(name='tryton_dv',
    version='1.3.0',
    description='Tryton development',
    long_description=read('README'),
    author='Datalife',
    url='http://www.datalifeit.es',
    download_url="https://gitlab.com/datalifeit/tryton-dvconfig",
    packages=find_packages(),
    package_data={},
    scripts=[],
    classifiers=[
        'Development Status :: 5 - Production/Stable',
        'Environment :: No Input/Output (Daemon)',
        'Intended Audience :: Developers',
        'License :: OSI Approved :: GNU General Public License (GPL)',
        'Operating System :: OS Independent',
        'Programming Language :: Python :: 2.7',
        'Programming Language :: Python :: 3.4',
        'Programming Language :: Python :: 3.5',
        'Programming Language :: Python :: 3.6',
        'Topic :: Software Development :: Libraries',
        ],
    license='GPL-3',
    install_requires=[
        'invoke>=1.0.0',
        'hgapi',
        'GitPython',
        'blessings',
        'polib',
        'requests',
        'cookiecutter',
        'configparser',
        'packaging'
        ],
    extras_require={},
    zip_safe=False,
    )
